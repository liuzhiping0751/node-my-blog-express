const path = require("path");
const fs = require('fs');
const shortid = require('shortid')

module.exports = (req, res, next) => {
    let fullPath = path.resolve('public', 'upload' + '/' + req.baseUrl) + '/';

    let {eBlog} = req.body;
    let fileName = shortid.generate() + '.md';

    fs.appendFile(fullPath + fileName, eBlog, (error) => {
        console.log(error);
        req.body.blog = fileName;
        eBlog = '';
        fileName = '';
        next();
    })

};
