const Menu = require('../model/Menu');
const Label = require('../model/Label');

module.exports = {
    // 登录信息回传
    async getUser(req, res, next) {
        req.user = req.session.user
        next();
    },

    /* 获取菜单 */
    async getMenu(req, res, next) {
        if (req.session.user) {
            req.menus = await Menu.getMenu(req.session.user.role.id);
            next();
        }
    },

    // 获取标签
    async getLabel(req, res, next) {
        req.labels = await Label.getLabel();
        next();
    },


    // 攔截部分管理員路由
    allowToAdmin(req, res, next) {
        const role = req.session.user.role;
        if (role) {
            if (role.id === 2) {
                next();
            } else {
                res.redirect('/');
            }
        } else {
            res.redirect('/blog.login');
        }

    }
}
