const Article = require('../model/Article');
const Label = require('../model/Label');


module.exports = {
    /* 获取最新前五的博文 */
    async getArticleNew(req, res, next) {
        req.ArticleNewList = await Article.getArticleNew(5);
        next();
    },

    /* 获取推荐的博文 */
    async getArticleRec(req, res, next) {
        req.ArticleRecList = await Article.getArticleRec(1);
        next();
    },

    /* 获取全部的推文 */
    async getArticleAll(req, res, next) {
        req.ArticleAllList = await Article.getAllArticle();
        next();
    },

    /* 获取全部的标签 */
    async getAllLabel(req, res, next) {
        req.Labels = await Label.getAllLabel();
        next();
    }


};
