-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: myblog
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8MB4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `als`
--

DROP TABLE IF EXISTS `als`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `als` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `ar_id` int NOT NULL COMMENT '文章编号',
  `la_id` int NOT NULL COMMENT '类目编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='文章标签表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `als`
--

LOCK TABLES `als` WRITE;
/*!40000 ALTER TABLE `als` DISABLE KEYS */;
/*!40000 ALTER TABLE `als` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '正文',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '发表时间',
  `hot` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:非热门 1:热门',
  `hits` int NOT NULL DEFAULT '0' COMMENT '点击量',
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `u_id` int NOT NULL COMMENT '用户id',
  `la_id` int NOT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='文章表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (34,'editor.md的使用','/upload/blog/Nha22naVi.md','2021-06-20 15:02:30',1,50,'/images/gallery-3.jpg',20,7),(35,'editor.md的使用1','/upload/blog/ydAwfM2C_.md','2021-06-20 15:44:28',0,0,'/images/gallery-2.jpg',20,11),(36,'editor.md的使用2','/upload/blog/Nha22naVi.md','2021-06-20 15:02:31',1,0,'/images/gallery-1.jpg',20,9),(40,'limit属于mysql的内置函数','/upload/blog/m_dVnOWX0.md','2021-06-20 15:02:31',0,0,'/upload/blog/photo-1624175291685.jpg',20,9),(41,'Node基础篇（文件操作）','/upload/blog/vF1YNOB7h.md','2021-06-20 15:32:12',1,0,'/upload/blog/photo-1624178902760.jpg',20,9),(42,'nodeJs中间件Multer详解','/upload/blog/vI66aKRB1.md','2021-06-20 15:02:31',0,0,'/upload/blog/photo-1624179343732.jpg',20,9),(43,'promise','/upload/blog/z8yB1YzsU.md','2021-06-20 15:56:25',1,0,'/upload/blog/photo-1624193239367.jpg',20,9);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labels`
--

DROP TABLE IF EXISTS `labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `la_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '类目名称',
  `la_describe` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '类目描述',
  `la_img` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '类目图片',
  `index` int NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='标签表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labels`
--

LOCK TABLES `labels` WRITE;
/*!40000 ALTER TABLE `labels` DISABLE KEYS */;
INSERT INTO `labels` VALUES (7,'#JAVA','Java是面向对象语言','/upload/label/photo-1624152690137.jpg',5),(8,'#PHP','Java是面向对象语言','/upload/label/photo-1624152697683.jpg',0),(9,'#Node.js','Java是面向对象语言','/upload/label/photo-1624152713109.jpg',0),(11,'#前端技术','234234','/upload/label/photo-1624152720393.jpeg',0),(12,'#jk','123456','/upload/label/photo-1624152728374.jpg',0),(14,'#sql','Sql','../upload/label/photo-1624175345876.jpg',0);
/*!40000 ALTER TABLE `labels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `m_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单名',
  `m_describe` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单描述',
  `m_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:不启用 1:启用',
  `m_url` varchar(100) NOT NULL COMMENT '菜单地址',
  `r_id` int NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (3,'用户管理','用户管理',1,'/admin/user.mgt',2),(4,'编写博客','编写博客',1,'/admin/release.mgt',2),(5,'编写博客','编写博客',1,'/admin/release.mgt',1),(6,'标签管理','标签管理',1,'/admin/label.mgt',2),(7,'博客管理','博客管理',1,'/admin/blog.mgt',2),(8,'轮播管理','轮播管理',1,'/admin/shuffling.mgt',2),(9,'相册管理','相册管理',1,'/admin/photo.mgt',2);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `p_img` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '相册图片',
  `index` int NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='相册表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,'/images/gallery-1.jpg',0),(2,'/images/gallery-2.jpg',0),(3,'/images/gallery-3.jpg',0),(4,'/upload/photo/photo-1624204635927.jpg',0),(7,'/upload/photo/photo-1624204661254.jpg',0),(8,'/upload/photo/photo-1624152469373.jpg',0);
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `r_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名',
  `r_describe` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色描述',
  `r_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:不启用 1:启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'普通用户','就是普通的用户',1),(2,'管理员','普普通通的管理员',1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shuff`
--

DROP TABLE IF EXISTS `shuff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shuff` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `sh_title` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '轮播图标题',
  `sh_img` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '轮播图图片',
  `index` int NOT NULL DEFAULT '0' COMMENT '排序，值越大越靠前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='轮播图表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shuff`
--

LOCK TABLES `shuff` WRITE;
/*!40000 ALTER TABLE `shuff` DISABLE KEYS */;
INSERT INTO `shuff` VALUES (14,'技术就是当你出生时尚不存在的任何事物。——Alan Kay','/upload/shuff/photo-1624202525825.jpg',0),(15,'计算机并不解决问题,它们只是执行解决方案。 ——Laurent Gasser','/images/img_bg_2.jpg',0),(16,'计算机使很多事情更容易做到,但其中大部分并不是必需。 ——Andy Rooney','/images/img_bg_3.jpg',0),(19,'do it let\'s do it','/upload/shuff/photo-1624202547993.jpeg',0);
/*!40000 ALTER TABLE `shuff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `userTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `avator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `salt` varchar(100) NOT NULL COMMENT '密码随机盐',
  `r_id` int NOT NULL COMMENT '角色id',
  `phone` varchar(100) NOT NULL COMMENT '手机号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_un` (`phone`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (20,'zhuim','$2b$06$5TbRisuJay1y2zmG1PO6FOuWGjpGoIdX8/f2W.4YtBbsxqKU0Nuj.','lishanweili@outlook.com','2021-06-19 07:22:13',NULL,'$2b$06$5TbRisuJay1y2zmG1PO6FO',2,'13737540751'),(21,'zhuimingweilai','$2b$06$gioUKXDxxl4XSUKDqvQJGeL/tH8jtCAjTmZGd.voGCglUTE0xFyKm','lishanweilai20@outlook.com','2021-06-19 04:31:50',NULL,'$2b$06$gioUKXDxxl4XSUKDqvQJGe',1,'13737540755'),(22,'1245','$2b$06$5TbRisuJay1y2zmG1PO6FOuWGjpGoIdX8/f2W.4YtBbsxqKU0Nuj.','2452740632@qq.com','2021-06-19 15:43:02',NULL,'$2b$06$5TbRisuJay1y2zmG1PO6FO',1,'13737540757');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'myblog'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-21  0:08:14
