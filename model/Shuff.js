const Model = require('./Model')

class Shuff extends Model {

    /* 获取全部的轮播图数据 */
    static getAllShuff() {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, sh_title, sh_img, `index` FROM shuff';
            this.query(sql, [])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 删除轮播图 */
    static delShuff(id) {
        return new Promise((resolve, reject) => {
            const sql = 'DELETE FROM myblog.shuff WHERE id=?'
            this.query(sql, [id])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 更新轮播图 */
    static upShuff(sh_title, sh_img, id) {
        return new Promise((resolve, reject) => {
            const sql = 'UPDATE shuff SET sh_title=?, sh_img=?, `index`=0 WHERE id=?';
            this.query(sql, [sh_title, sh_img, id])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 新增轮播图 */
    static inShuff(sh_title, sh_img) {
        return new Promise((resolve, reject) => {
            const sql = 'INSERT INTO shuff (sh_title, sh_img, `index`) VALUES(?, ?, 0)';
            this.query(sql, [sh_title, sh_img])
                .then(resolve)
                .catch(reject)
        });
    }

}

module.exports = Shuff;
