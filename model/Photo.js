const Model = require('./Model')

class Photo extends Model {
    /* 获取全部的图片数据 */
    static getAllPhoto() {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, p_img, `index` FROM photos';
            this.query(sql, [])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 删除图片 */
    static delPhoto(id) {
        return new Promise((resolve, reject) => {
            const sql = 'DELETE FROM photos WHERE id=?'
            this.query(sql, [id])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 更新图片 */
    static upPhoto(p_img, id) {
        return new Promise((resolve, reject) => {
            const sql = 'UPDATE photos SET p_img=?, `index`=0 WHERE id=?';
            this.query(sql, [p_img, id])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 新增图片 */
    static inPhoto(p_img) {
        return new Promise((resolve, reject) => {
            const sql = 'INSERT INTO photos (p_img, `index`) VALUES(?, 0)';
            this.query(sql, [p_img])
                .then(resolve)
                .catch(reject)
        });
    }
}

module.exports = Photo;
