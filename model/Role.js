const Model = require('./Model');

class Role extends Model {
    // 查询用户角色
    static getRole(id) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, r_name, r_describe, r_state FROM roles WHERE id = ?'
            this.query(sql, [id])
                .then(resolve)
                .catch(reject);
        });
    }
}

module.exports = Role;
