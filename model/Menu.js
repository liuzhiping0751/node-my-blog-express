const Model = require('./Model')

class Menu extends Model {
    // 查询用户菜单
    static getMenu(id) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, m_name, m_describe, m_state, m_url FROM menus WHERE r_id=?'
            this.query(sql, [id])
                .then(resolve)
                .catch(reject)
        })
    }
}

module.exports = Menu
