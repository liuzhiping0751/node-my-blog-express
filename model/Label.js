const Model = require('./Model')

class Label extends Model {

    /* 获取全部的标签数据 */
    static getAllLabel() {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, la_name, la_describe, la_img, `index` FROM labels';
            this.query(sql, [])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 获取下拉选项 */
    static getLabel() {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, la_name FROM labels';
            this.query(sql, [])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 删除标签 */
    static delLabel(id) {
        return new Promise((resolve, reject) => {
            const sql = 'DELETE FROM labels WHERE id=?'
            this.query(sql, [id])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 更新标签 */
    static upLabel(la_name,la_describe,la_img,id) {
        return new Promise((resolve, reject) => {
            const sql = 'UPDATE labels SET la_name=?, la_describe=?, la_img=? WHERE id=?';
            this.query(sql, [la_name,la_describe,la_img,id])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 新增标签 */
    static inLabel(la_name,la_describe,la_img) {
        return new Promise((resolve, reject) => {
            const sql = 'INSERT INTO labels (la_name, la_describe, la_img, `index`) VALUES(?, ?, ?, 0)';
            this.query(sql, [la_name,la_describe,la_img])
                .then(resolve)
                .catch(reject)
        });
    }

    /* 查询标签名 */
    static getLabelName(id) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, la_name FROM labels WHERE id = ?'
            this.query(sql, [id])
                .then(resolve)
                .catch(reject);
        });
    }
}

module.exports = Label;



