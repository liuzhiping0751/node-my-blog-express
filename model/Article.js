const Model = require('./Model')

class Article extends Model {
    //保存文章
    static saveArticle(title, content, hot, thumbnail, u_id, la_id) {
        return new Promise((resolve, reject) => {
            const sql = 'INSERT INTO myblog.article (title, content, `time`, hot, hits, thumbnail, u_id, la_id) VALUES(?, ?, CURRENT_TIMESTAMP, ?, 0, ?, ?, ?)'
            this.query(sql, [title, content, hot, thumbnail, u_id, la_id])
                .then(resolve)
                .catch(reject);
        });
    }

    //查詢該用戶下的文章
    static getArticle(u_id) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, thumbnail, la_id FROM article WHERE u_id=?';
            this.query(sql, [u_id])
                .then(resolve)
                .catch(reject);
        });
    }

    //獲取全部文章
    static getAllArticle() {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, thumbnail, la_id FROM article ';
            this.query(sql, [])
                .then(resolve)
                .catch(reject);
        });
    }

    // 点击量
    static addHits(id) {
        return new Promise((resolve, reject) => {
            const sql = 'update article set hits=hits + 1 where id=?';
            this.query(sql, [id])
                .then(resolve)
                .catch(reject);
        });
    }

    //刪除文章
    static delArticle(id) {
        return new Promise((resolve, reject) => {
            const sql = 'DELETE FROM article WHERE id=?';
            this.query(sql, [id])
                .then(resolve)
                .catch(reject);
        });

    }

    //更新文章
    static upArticle(title, content, hot, thumbnail, la_id, id) {
        return new Promise((resolve, reject) => {
            const sql = 'UPDATE article SET title=?, content=?,  hot=?,  thumbnail=?,la_id=? WHERE id=?';
            this.query(sql, [title, content, hot, thumbnail, la_id, id])
                .then(resolve)
                .catch(reject);
        });
    }

    // 通過id獲取文章
    static getArticleId(id) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, u_id, thumbnail, la_id FROM article WHERE id=?';
            this.query(sql, [id])
                .then(resolve)
                .catch(reject);
        });
    }

    /* 查询被推荐文章 */
    static getArticleRec(hot) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, u_id, thumbnail, la_id FROM article WHERE hot=?';
            this.query(sql, [hot])
                .then(resolve)
                .catch(reject);
        });
    }

    /* 查询某标签下的文章 */
    static getArticleLabel(la_id) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, u_id, thumbnail, la_id FROM article WHERE la_id=?';
            this.query(sql, [la_id])
                .then(resolve)
                .catch(reject);
        });
    }

    /* 查询多条件文章 */
    static getArticles(la_id, hot, u_id) {
        let sql, arr;
        if (la_id && hot) {
            sql = 'SELECT id, title, content, `time`, hot, hits, u_id, thumbnail, la_id FROM article WHERE la_id=? and hot=? and u_id=?';
            arr = [la_id, hot, u_id];
        } else {
            if (la_id) {
                sql = 'SELECT id, title, content, `time`, hot, hits, u_id, thumbnail, la_id FROM article WHERE la_id=? and u_id=?';
                arr = [la_id, u_id];
            } else {
                sql = 'SELECT id, title, content, `time`, hot, hits, u_id, thumbnail, la_id FROM article WHERE hot=? and u_id=?';
                arr = [hot, u_id];
            }
        }
        return new Promise((resolve, reject) => {
            this.query(sql, arr)
                .then(resolve)
                .catch(reject);
        });
    }

    /* 查询最新的N条文章 */
    static getArticleNew(num) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, thumbnail, la_id FROM article order by time desc limit ?';
            this.query(sql, [num])
                .then(resolve)
                .catch(reject);
        })
    }

    // 搜索用
    static likeArticle(str) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT id, title, content, `time`, hot, hits, thumbnail, la_id FROM article Where title like ?';
            this.query(sql, ['%' + str + '%'])
                .then(resolve)
                .catch(reject);
        });

    }

}


module.exports = Article
