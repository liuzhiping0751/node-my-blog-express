const express = require('express');
const router = express.Router();
const blogFile = require('../middleware/blogFile');
const auth = require('../middleware/auth');
const Article = require('../model/Article');
const uploadFile = require('../middleware/multer');
const fileTools = require('../tools/fsTools');


/* 获取编写的博客 */
router.post('/getEditBlog', blogFile, (req, res) => {

    console.log(req.body.blog);

    res.send({msg: ''});
});

/* 查询被推荐文章 */
router.post('/getArticleRec', async (req, res) => {
    const {hot} = req.body;
    const ArticleList = await Article.getArticleRec(hot);
    res.send({msg: 's', ArticleList: ArticleList});
});


/* 查询某标签下的文章 */
router.post('/getArticleLabel', async (req, res) => {
    const {la_id} = req.body;
    const ArticleList = await Article.getArticleLabel(la_id);
    res.send({msg: 's', ArticleList: ArticleList});
});

// 文章管理搜索用
router.post('/getArticles', auth.getLabel, async (req, res) => {

    const {la_id, hot} = req.body;
    const ArticleList = await Article.getArticles(la_id, hot, req.session.user.user.id);
    res.send({msg: 's', ArticleList: ArticleList, labels: req.labels});
});

/* 获取自己发布的所有文章 */
router.post('/getAllArticle', auth.getLabel, async (req, res) => {
    const ArticleList = await Article.getArticle(req.session.user.user.id);
    res.send({msg: 's', ArticleList: ArticleList, labels: req.labels});
});

/* 删除文章 */
router.post('/delArticle', async (req, res) => {
    const {id, content, thumbnail} = req.body;
    const statue = await Article.delArticle(id);
    fileTools.delFile(content);
    fileTools.delFile(thumbnail);
    res.send({msg: 's'});
});

/* 新增文章 */
router.post('/saveArticle', [uploadFile, blogFile], async (req, res, next) => {

    const row = JSON.parse(req.body.row);

    const photo = req.body.photo;
    const blog = req.body.blog;
    let thumbnail = '/images/img-1.jpg';
    const content = '/upload' + req.baseUrl + '/' + blog;
    if (photo || photo !== "") {
        thumbnail = '/upload' + req.baseUrl + '/' + photo;
    }
    const u_id = req.session.user.user.id;
    const statue = await Article.saveArticle(row.title, content, row.hot, thumbnail, u_id, row.la_id);

    res.send({msg: 's'});
});

/* 修改文章 */
router.post('/upArticle', [uploadFile, blogFile], async (req, res, next) => {

    let {id, title, hot, thumbnail, la_id, content} = JSON.parse(req.body.row);

    const photo = req.body.photo;
    const blog = req.body.blog;

    if (blog || blog !== "") {
        fileTools.delFile(content);
        content = '/upload' + req.baseUrl + '/' + blog;
    }
    if (photo || photo !== "") {
        fileTools.delFile(thumbnail);
        thumbnail = '/upload' + req.baseUrl + '/' + photo;
    }
    const statue = await Article.upArticle(title, content, hot, thumbnail, la_id, id);

    res.send({msg: 's'});
});


/* 编辑文章 */
router.get('/toEditBlog/:id', [auth.getMenu, auth.getLabel], async (req, res, next) => {
    const Articles = await Article.getArticleId(req.params.id);
    const {menus, labels} = req;
    res.render('admin/submit_blog.ejs', Object.assign({title: '发表博客', Articles: Articles}, {menus, labels}));
});


module.exports = router;
