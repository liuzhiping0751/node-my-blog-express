const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth.getMenu, function (req, res, next) {

    res.render('admin/index.ejs', {title: '后台管理', menus: req.menus});
});

router.get('/user.mgt', [auth.getMenu, auth.allowToAdmin], function (req, res, next) {

    res.render('admin/user_admin.ejs', {title: '用户管理', menus: req.menus});
});

router.get('/release.mgt', [auth.getMenu, auth.getLabel], function (req, res, next) {
    const {menus, labels} = req;
    res.render('admin/submit_blog.ejs', Object.assign({title: '编写博客'}, {menus, labels}));
});

router.get('/label.mgt', [auth.getMenu, auth.allowToAdmin], function (req, res, next) {

    res.render('admin/label_admin.ejs', {title: '标签管理', menus: req.menus});
});

router.get('/blog.mgt', [auth.getMenu, auth.getLabel], function (req, res, next) {

    const {menus, labels} = req;
    res.render('admin/blog_admin.ejs', Object.assign({title: '博客管理'}, {menus, labels}));
});

router.get('/shuffling.mgt', [auth.getMenu, auth.allowToAdmin], function (req, res, next) {

    res.render('admin/shuffling.ejs', {title: '轮播管理', menus: req.menus});
});

router.get('/photo.mgt', [auth.getMenu, auth.allowToAdmin], function (req, res, next) {

    res.render('admin/picture.ejs', {title: '相册管理', menus: req.menus});
});


module.exports = router;
