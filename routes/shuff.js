const express = require('express');
const router = express.Router();
const Shuff = require('../model/Shuff');
const uploadFile = require('../middleware/multer')
const fileTools = require('../tools/fsTools');

/* 获取全部轮播 */
router.post('/shuff.getAll', async (req, res, next) => {
    const shuffList = await Shuff.getAllShuff();
    if (shuffList) {
        res.send({msg: 's', shuffList: shuffList});
    } else {
        res.send({msg: 'f'});
    }
});

router.post('/shuff.del', async (req, res, next) => {

    const {id, sh_img} = req.body;
    const statue = await Shuff.delShuff(id);
    if (statue) {
        fileTools.delFile(sh_img);
        res.send({msg: 's'});
    } else {
        res.send({msg: 'f'});
    }

});


//使用uploadFile中间件
router.post("/shuff.up", uploadFile, async (req, res) => {

    const row = JSON.parse(req.body.row);
    const photo = req.body.photo;
    let sh_img = row.sh_img;
    if (photo || photo !== "") {
        fileTools.delFile(sh_img);
        sh_img = '/upload' + req.baseUrl + '/' + req.body.photo;
    }

    const statue = await Shuff.upShuff(row.sh_title, sh_img, row.id);

    console.log('statue---------------->>' + JSON.stringify(statue));
    res.send({msg: "文件上传成功"});
})


router.post("/shuff.add", uploadFile, async (req, res) => {
    const row = JSON.parse(req.body.row);

    const photo = req.body.photo;
    let sh_img = '/images/img-1.jpg';
    if (photo || photo !== "") {
        sh_img = '/upload' + req.baseUrl + '/' + req.body.photo;
    }
    const statue = await Shuff.inShuff(row.sh_title, sh_img);
    console.log('statue---------------->>' + JSON.stringify(statue));
    res.send({msg: "文件上传成功"});
})


module.exports = router;
