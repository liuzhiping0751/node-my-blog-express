const express = require('express');
const router = express.Router();
const User = require('../model/User');


/* 查询所有用户 */
router.post('/users.get', async (req, res, next) => {
    const userList = await User.getAllUser();
    if (userList) {
        res.send({msg: 's', userList: userList});
    } else {
        res.send({msg: 'f'});
    }

});

/* 重置密码 */
router.post('/users.reset', async (req, res, next) => {
    const id = req.body.id;
    const statue = await User.rePassword('$2b$06$5TbRisuJay1y2zmG1PO6FOuWGjpGoIdX8/f2W.4YtBbsxqKU0Nuj.','$2b$06$5TbRisuJay1y2zmG1PO6FO',id)
    res.send({mes:statue})
});

/* 修改用户资料 */
router.post('/users.update', async (req, res, next) => {

    const {username, email, phone} = req.body

    const statue = await User.reInformation(username, email, phone)

    res.send({mes: statue})
});



/* 删除用户 */
router.post('/users.del', async (req, res, next) => {
    const {id} = req.body;
    const statue = await User.delUser(id);
    res.send({mes: statue})
});


module.exports = router;
