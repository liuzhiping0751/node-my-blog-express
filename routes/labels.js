const express = require('express');
const router = express.Router();
const Label = require('../model/Label');
const uploadFile = require('../middleware/multer');
const fileTools = require('../tools/fsTools');

/* 获取全部标签 */
router.post('/label.getAll', async (req, res, next) => {
    const labelList = await Label.getAllLabel();
    if (labelList) {
        res.send({msg: 's', labelList: labelList});
    } else {
        res.send({msg: 'f'});
    }
});
//删除标签
router.post('/label.del', async (req, res, next) => {

    const {id,la_img} = req.body;
    const statue = await Label.delLabel(id);
    if (statue) {
        fileTools.delFile(la_img);
        res.send({msg: 's'});
    } else {
        res.send({msg: 'f'});
    }


});

//修改标签
router.post("/multer/up", uploadFile, async (req, res) => {

    const row = JSON.parse(req.body.row);
    const photo = req.body.photo;
    let la_img = row.la_img;
    if (photo || photo !== "") {
        // 上传完文件后删除原文件
        fileTools.delFile(la_img);
        la_img = '/upload' + req.baseUrl +'/'+ req.body.photo;
    }


    const statue = await Label.upLabel(row.la_name, row.la_describe, la_img, row.id);

    console.log('statue---------------->>' + JSON.stringify(statue))
    res.send({msg: "文件上传成功"});
})

router.post("/multer/add", uploadFile, async (req, res) => {
    const row = JSON.parse(req.body.row);

    const photo = req.body.photo;
    let la_img = '/images/img-1.jpg';
    if (photo || photo !== "") {
        la_img = '/upload' + req.baseUrl +'/'+ req.body.photo;
    }

    const statue = await Label.inLabel(row.la_name, row.la_describe, la_img);
    res.send({msg: "文件上传成功"});
})


module.exports = router;
