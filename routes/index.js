const express = require('express');
const router = express.Router();
const User = require('../model/User');
const Role = require('../model/Role');
const auth = require('../middleware/auth');
const hash = require('../tools/passHash');
const article = require('../middleware/article');
const Article = require('../model/Article');
const LabelMapper = require('../model/Label');

/* GET home page. */
router.get('/', [auth.getUser, article.getArticleRec, article.getArticleNew, article.getAllLabel], (req, res, next) => {
    const {ArticleRecList, ArticleNewList, user, Labels} = req;

    res.render('index.ejs', Object.assign({isActive: 'isActive'}, {ArticleRecList, ArticleNewList, user, Labels}));
});
// 获取所有文章
router.get('/blog.list', [auth.getUser, article.getArticleAll, article.getArticleRec], (req, res, next) => {
    const {user, ArticleAllList, ArticleRecList} = req;
    res.render('blog.ejs', Object.assign({isActive: 'isBlogActive'}, {user, ArticleAllList, ArticleRecList}));
});
// 获取所有标签
router.get('/blog.portfolio', [auth.getUser, article.getAllLabel, article.getArticleRec], (req, res, next) => {
    const {Labels, ArticleRecList, user} = req;
    res.render('portfolio.ejs', Object.assign({isActive: 'isPortfolioActive'}, {Labels, ArticleRecList, user}));
});
//登录
router.get('/blog.login', (req, res, next) => {
    res.render('login.ejs', {title: '登录'});
});
// 登录业务
router.post('/blog.login', async (req, res) => {
    const {phone, password} = req.body;

    const user = (await User.doLogin(phone))[0];
    if (user) {
        /* 认证用户密码 */
        hash.authCode(password, user.password, async (fal) => {
            if (fal) {
                const role = (await Role.getRole(user.r_id))[0];
                req.session.user = {
                    user: {id: user.id, username: user.username},
                    role: {id: role.id, r_name: role.r_name}
                };
                res.send({msg: 's'});
            } else {
                res.send({msg: 'f'});
            }
        });

    } else {
        res.send({msg: 'f'});
    }
})
//跳转注册页
router.get('/blog.signup', (req, res, next) => {
    res.render('signup.ejs', {title: '注册'});
});
// 注册
router.post('/blog.signup', async (req, res, next) => {
    const {username, password, email, phone} = req.body;
    const sta = (await User.toRegister(username, password, email, phone));
    res.send({msg: sta.affectedRows, errno: sta.errno});
});
/* 获取文章 */
router.get('/article/:id', [auth.getUser, article.getArticleRec, article.getArticleNew, article.getAllLabel], async (req, res, next) => {
    const Articles = await Article.getArticleId(req.params.id);

    if (Articles[0]) {
        const {la_id, u_id} = Articles[0];
        const {ArticleRecList, ArticleNewList, user, Labels} = req;

        //记录用户点击文章量
        if (user) {
            const upHits = Article.addHits(req.params.id);//点击量
            //console.log('----------->>>',upHits);
        }
        const aLabel = await LabelMapper.getLabelName(la_id);// 获取文章标签
        const aUser = await User.selUser(u_id);// 获取文章标签
        res.render('post.ejs', Object.assign({
            isActive: 'isBlogActive',
            Articles: Articles,
            ALabel: aLabel,
            AUser: aUser
        }, {
            ArticleRecList,
            ArticleNewList,
            user,
            Labels
        }));
    } else {
        res.redirect('/');
    }
});

/* 获取标签下的文章 */
router.get('/label/:id', [auth.getUser, article.getArticleRec], async (req, res, next) => {
    const Articles = await Article.getArticleLabel(req.params.id)
    const LabelName = await LabelMapper.getLabelName(req.params.id);
    const {user, ArticleRecList} = req;
    res.render('blog.ejs', Object.assign({
        isActive: 'isBlogActive',
        ArticleAllList: Articles,
        LabelName: LabelName[0]
    }, {user, ArticleRecList}));
});

/* 获取搜索下的文章 */
router.get('/search/:title', [auth.getUser, article.getArticleRec], async (req, res, next) => {
    const seaTitle = req.params.title;
    const Articles = await Article.likeArticle(seaTitle);
    const {user, ArticleRecList} = req;
    res.render('blog.ejs', Object.assign({
        isActive: 'isBlogActive',
        ArticleAllList: Articles,
        LabelName: {la_name: seaTitle}
    }, {user, ArticleRecList}));
});


// 登出
router.get('/blog.logout', (req, res) => {
    req.log = {handle: '退出'}
    req.session.user = ''
    res.redirect('/')
})

module.exports = router;
