const fs = require('fs');
const path = require('path');


module.exports = {
    // 删除上传的文件
    delFile(imgUrl) {
        let fullPath = path.resolve(__dirname, "../public" + imgUrl);
        fs.unlink(fullPath, err => {
            if (!err) console.log("删除成功");
        });
    }
}
