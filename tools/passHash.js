const bcrypt = require("bcrypt");

module.exports = {
    /* 获取密文和盐 */
    getPassword(pas, len, cb) {
        bcrypt.genSalt(len, (err, salt) => {
            if (err) return console.error(err);

            bcrypt.hash(pas, salt, (hashErr, cipher) => {
                if (hashErr) return console.error(hashErr);

                cb(cipher, salt);
            });
        });
    },
    /* 密码校验 */
    authCode(pass, cipher, cb) {
        bcrypt.compare(pass, cipher, (err, fal) => {
            if (err) return console.error(err);
            cb(fal);
        });
    }
};

