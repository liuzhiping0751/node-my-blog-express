# node-myBlog-express

#### 介绍
基于Node，express的个人博客（新手练手）

#### 软件架构
node.js
express
vue.js
mysql


#### 安装教程

1.  导入mysql数据
2.  安装node依赖
3.  安装nodemon

#### 使用说明

1.  npm run start



#### 账号
账号：123456789
密码：123456
*如果没有权限可以自行修改数据库文件
