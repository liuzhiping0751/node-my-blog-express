# limit属于mysql的内置函数，能够返回特定行号或行数的数据

常用语法

```sql
limit m,n;　　　　#意思是从m+1行开始，返回n行。或者返回m行后的n行
```
> ### #返回0行之后的两行，不包括0行

```sql
select * from info_duty limit 0,2;
```
> ### #从第三行开始，返回2行

```sql
select * from info_duty limit 2,2;
```
如果只有一个参数，则表示从第一行开始返回n行

> ### #从第一行开始返回两行，相当于limit 0,2

```sql
select * from info_duty limit 2;
```